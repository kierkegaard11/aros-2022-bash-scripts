#!/bin/bash

#
#    Usage: javio [OPTION]... [FILE]...
#                  
#	se		upis novog studenta
#	-l		ispis broja javljanja i imena studenata
#	-r		kraj casa, brisanje liste
#

#	Kako bi mogli da pokrenete program kao komandu u terminalu "$ javio se"
#		potrebno je napraviti alias javio='/put/do/skripte/poeni.sh'

lista="lista.info"

if [ ! -f $lista ]
then
	touch $lista
	echo -e "Ime\tIndeks" > $listacase 
fi;

case $1 in
	"se") 
		read -p "Ime: " name
		read -p "Indeks: " indeks
		echo -e "$name\t$indeks" >> $lista
	;;
	"-l") 
		result=$(cat lista.info | cut -d' ' -f 1,2 | tail -n +2 | sort | uniq -c | sort -r)
		echo "$result"
	;;
	"-r") 
		> $lista
		echo -e "Ime\tIndeks" > $lista
	;;
	*)
		echo -e "\n"
		echo "Usage: javio [OPTION]... [FILE]..."
		echo -e "\n"
		echo "   se              upis novog studenta"
		echo "   -l              ispis broja javljanja i imena studenata"
		echo "   -r              kraj casa, brisanje liste"
		echo -e "\n"
	;;
esac

